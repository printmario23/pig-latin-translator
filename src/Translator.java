
public class Translator {

	private String phrase;
	public static final String NIL = "nil";

	public Translator(String inputPhrase) {
		this.phrase = inputPhrase;
	}

	public String getPhrase() {
		return phrase;
	}
	
	
	public String translate() {
		if(!phrase.equals("")) {
			StringBuilder outputPhrase = new StringBuilder();
            
			String[] splits = phrase.split(" ");
			for(int i=0; i<splits.length; i++){
				phrase = splits[i];
				
				String[] composite = phrase.split("-");
				for(int j=0; j<composite.length; j++){
					phrase = composite[j];
					
					outputPhrase.append( new Word(phrase).worldTranslation() );
					
					if(j<composite.length-1) {
						outputPhrase.append("-");
					}
				}

				if(i<splits.length-1) {
					outputPhrase.append(" ");
				}
			}
			return outputPhrase.toString();
		}
		return NIL;
	}

}

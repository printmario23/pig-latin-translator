
public class Word {
	
	private String phrase;
	public static final String NIL = "nil";
	private String allowedPunctuations =  ".,;:?!\'()";
	
	public Word(String inputPhrase) {
		this.phrase = inputPhrase;
	}
	
	public String getPhrase() {
		return phrase;
	}

	public boolean startsWithVowel() {
		return this.phrase.startsWith("a") || this.phrase.startsWith("e") || this.phrase.startsWith("i") || this.phrase.startsWith("o") || this.phrase.startsWith("u")
				|| this.phrase.startsWith("A") || this.phrase.startsWith("E") || this.phrase.startsWith("I") || this.phrase.startsWith("O") || this.phrase.startsWith("U");
	}
	
	public boolean endsWithVowel() {
		return this.phrase.endsWith("a") || this.phrase.endsWith("e") || this.phrase.endsWith("i") || this.phrase.endsWith("o") || this.phrase.endsWith("u")
				|| this.phrase.endsWith("A") || this.phrase.endsWith("E") || this.phrase.endsWith("I") || this.phrase.endsWith("O") || this.phrase.endsWith("U") ;
	}

	public boolean isVowel(char c) {
		return c=='a' || c=='e' || c=='i' || c=='o' || c=='u'|| c=='A'|| c=='E'|| c=='I'|| c=='O'|| c=='U';
	}
	
	public int firstsConsonant() {
		int cCount = 0;
		for(int i=0; i<phrase.length(); i++) {
			if(!isVowel(phrase.charAt(i))) {
				cCount++;
			}else {
				return cCount;
			}
		}
		return cCount;
	}
	
	public boolean hasPunctuation() {
		for(int i=0; i<phrase.length(); i++) {
			for(int j=0; j<allowedPunctuations.length(); j++){
				if(phrase.charAt(i)==allowedPunctuations.charAt(j)) {
					return true;
				}
			}
		}
		return false;
	}
	
	public String worldTranslation() {
		if(!phrase.equals("")) {
			String translation="";
			String punctuation="";
			
			if(hasPunctuation()) {
				punctuation = Character.toString(phrase.charAt(phrase.length()-1));
				phrase = phrase.substring(0,phrase.length()-1);
			}
			
			if(startsWithVowel()) {
				if(phrase.endsWith("y")) {
					translation += phrase + "nay";
				}else if(endsWithVowel()) {
					translation = phrase + "yay"; 
				}else{
					translation = phrase + "ay";
				}
				
			}else if(!startsWithVowel()) {
				translation = phrase.substring(firstsConsonant()) + phrase.substring(0, firstsConsonant()) + "ay";
			}
			
			return translation + punctuation;
		}
		return NIL;
	}
}
